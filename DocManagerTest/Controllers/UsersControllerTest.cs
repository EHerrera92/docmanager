﻿using DocManager.Controllers;
using DocManager.Entity;
using DocManager.Repositories.Contracts;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Xunit;
using Microsoft.AspNetCore.Mvc;

namespace DocManagerTest
{
	public class UsersControllerTest
	{
		[Fact]
		public async Task UserRemoveAndRead()
		{
			var mockUserRepo = GetMockUserRepo();
			UsersController uc = new UsersController(mockUserRepo, null);
			var insertResult = await uc.AddOneUser(new DocManager.Domain.User { Id = 127123L, Name = "Bob", Username = "Boby123", Password = "asdfasdf" }) as CreatedAtActionResult;
			Assert.Equal(201, insertResult.StatusCode);
			await uc.DeleteOneUser(127123L);
			var notFoundResult = await uc.GetOneUser(127123L) as NotFoundResult;
			Assert.Equal(404, notFoundResult.StatusCode);
		}

		[Fact]
		public async Task UserInsertWithMissingUsername()
		{
			var mockUserRepo = GetMockUserRepo();
			UsersController uc = new UsersController(mockUserRepo, null);
			var badResult = await uc.AddOneUser(new DocManager.Domain.User { Username = "", Name = "Blank Username", Password = "Password" }) as BadRequestObjectResult;
			Assert.Equal(400, badResult.StatusCode);
		}

		[Fact]
		public async Task UserUpdateAndRead()
		{
			var mockUserRepo = GetMockUserRepo();
			UsersController uc = new UsersController(mockUserRepo, null);
			await uc.UpdateUser(new DocManager.Domain.User { Id = 2L, Name = "Ricardo", Username = "ricardo", Password = null }, 2L);
			var result = (await uc.GetOneUser(2L)) as OkObjectResult;
			Assert.Equal(200, result.StatusCode);
			var resultObject = (DocManager.Domain.User)result.Value;
			Assert.Equal("Ricardo", resultObject.Name);
			Assert.Equal("ricardo", resultObject.Username);
			Assert.Equal(2L, resultObject.Id);
			Assert.Null(resultObject.Password);
		}

		[Fact]
		public async Task UsertInsertAndRead()
		{
			var mockUserRepo = GetMockUserRepo();
			UsersController uc = new UsersController(mockUserRepo, null);
			await uc.AddOneUser(new DocManager.Domain.User { Name = "Bob", Password = "password", Username = "BoBy123" });
			var result = (await uc.GetOneUser(127123L)) as OkObjectResult;
			Assert.Equal(200, result.StatusCode);
			var resultObject = (DocManager.Domain.User)result.Value;
			Assert.Equal("Bob", resultObject.Name);
			Assert.Equal("boby123", resultObject.Username);
			Assert.Equal(127123L, resultObject.Id);
			Assert.Null(resultObject.Password);
		}

		[Fact]
		public async Task UserInsertWithDuplicateUsername()
		{
			var mockUserRepo = GetMockUserRepo();
			UsersController uc = new UsersController(mockUserRepo, null);
			await uc.AddOneUser(new DocManager.Domain.User { Name = "Bob", Password = "password", Username = "BoBy123" });
			var result = (await uc.GetOneUser(127123L)) as OkObjectResult;
			Assert.Equal(200, result.StatusCode);
			var badResult = await uc.AddOneUser(new DocManager.Domain.User { Name = "Rex", Password = "password", Username = "boby123" }) as BadRequestObjectResult;
			Assert.Equal(400, badResult.StatusCode);
		}

		[Fact]
		public async Task UserInsertWithInvalidCharacters()
		{
			var mockUserRepo = GetMockUserRepo();
			UsersController uc = new UsersController(mockUserRepo, null);
			var badResult = await uc.AddOneUser(new DocManager.Domain.User { Name = "Bob", Password = "password", Username = "BoBy$123" }) as BadRequestObjectResult;
			Assert.Equal(400, badResult.StatusCode);
		}

		private IUserRepository GetMockUserRepo()
		{
			List<User> userTable = new List<User>();
			userTable.Add(new User { Username = "admin", Name = "Administrator", Id = 1L });
			userTable.Add(new User { Username = "manager", Name = "Manager", Id = 2L });
			userTable.Add(new User { Username = "user", Name = "User", Id = 3L });
			userTable.Add(new User { Username = "noaccess", Name = "No Access", Id = 4L });

			Mock<IUserRepository> mockUserRepo = new Mock<IUserRepository>();
			mockUserRepo.Setup(mr => mr.HasPrivilegeAsync(It.IsAny<long>(), It.IsAny<IUserRepository.PrivilegeType>())).Returns((Func<long, IUserRepository.PrivilegeType, Task<bool>>)(async (a, b) => {
				return true;
			}));

			mockUserRepo.Setup(mr => mr.GetByUsernameAsync(It.IsAny<string>())).Returns((Func<string, Task<User>>)(async (u) => {
				return userTable.Where(rec => rec.Username.ToLower() == u.ToLower()).FirstOrDefault();
			}));

			mockUserRepo.Setup(mr => mr.UpdateAsync(It.IsAny<User>())).Returns((Func<User, Task>)(async (u) => {
				var ent = userTable.Where(rec => rec.Id == u.Id).FirstOrDefault();
				ent.Name = u.Name;
				ent.Username = u.Username;
				ent.PasswordHash = u.PasswordHash;
				ent.PasswordSalt = u.PasswordSalt;
			}));

			mockUserRepo.Setup(mr => mr.GetAsync(It.IsAny<long>())).Returns((Func<long, Task<User>>)(async (userId) => {
				return userTable.Where(rec => rec.Id == userId).FirstOrDefault();
			}));

			mockUserRepo.Setup(mr => mr.InsertAsync(It.IsAny<User>())).Returns((Func<User, Task<User>>)(async (u) => {
				u.Id = 127123L;
				userTable.Add(u);
				return u;
			}));

			mockUserRepo.Setup(mr => mr.DeleteAsync(It.IsAny<long>())).Returns((Func<long, Task>)(async (userId) => {
				userTable.RemoveAll(u => u.Id == userId);
			}));

			return mockUserRepo.Object;
		}
	}
}
