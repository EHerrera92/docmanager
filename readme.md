# DocManager


## Tables

User - Login information for each user

Group - A collection of privileges. Users can belong to multiple groups

Document - Metadata and binary data for uploaded documents

Privilege - Controls access to a resource. Groups can have multiple privileges

UserGroup - Link between User and Group

GroupPrivilege - Link between Group and Privilege

## Projects

DocManager - Main API 

DocManagerTest - Unit Tests

DocManagerE2E - End-to-End Tests

## Database Setup
Adjust Postgres connection string in appsettings.json to use correct host and user credentials. Example: 

```bash
Host=localhost;Database=postgres;Username=docuser;Password=admin;SearchPath=docmanager
```

Create schema
```bash
dotnet ef database update Initial
```

Then run Init.sql on the database to seed initial group and user data and to create the stored procedure.

## Examples

Get all users
```bash
GET /users
```

Get specific user
```bash
GET /users/3
```

Delete a user
```bash
DELETE /users/3
```

Add a user
```bash
POST /users
{
  "Name": "Bob",
  "Username": "BobTheBuilder",
  "Password": "Password"
}
```

Give user membership to a group
```bash
POST /users/5/groups
{
  "Id": 6
}
```

Remove user membership from a group
```bash
DELETE /users/5/groups/6
```

Retrieve all groups the user is a member of
```bash
GET /users/5/groups
```

Update user. Set password to null unless updating password
```bash
PUT /users/5
{
  "Name": "Bob Brown",
  "Username": "BobTheBuilder",
  "Password": null
}
```

Retrieve metadata for documents uploaded by given user
```bash
GET /users/5/documents
```

Upload a document
```bash
POST /documents
```

Download a document with metadata
```bash
GET /documents/10
```

Retrieve a list of groups
```bash
GET /groups
```

Retrieve a specific group
```bash
GET /groups/10
```

Remove a group
```bash
DELETE /groups/10
```

Add a new group
```bash
POST /groups
```

Add privilege to a group
```bash
POST /groups/10/privileges
```

Remove privilege from a group
```bash
DELETE /groups/10/privileges/12
```

Retrieve privileges for the given group
```bash
GET /groups/10/privileges
```

Update a group
```bash
PUT /groups/10
{
  "Id": "10",
  "Name": "Curator"
}
```

Retrieve a list of all privileges in the system
```bash
GET /privileges
```
