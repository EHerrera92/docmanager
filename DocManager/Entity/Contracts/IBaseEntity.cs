﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Entity.Contracts
{
	public interface IBaseEntity
	{
		long Id { get; set; }
		DateTime? CreatedDate { get; set; }
		DateTime? ModifiedDate { get; set; }
	}
}
