﻿using System;
using DocManager.Entity.Contracts;

namespace DocManager.Entity
{
	public class Group : IBaseEntity
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public DateTime? CreatedDate { get; set; }
		public DateTime? ModifiedDate { get; set; }
	}
}
