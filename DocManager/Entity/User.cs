﻿using DocManager.Entity.Contracts;
using System;

namespace DocManager.Entity
{
	public class User : IBaseEntity
	{
		public long Id { get; set; }
		public string Username { get; set; }
		public string PasswordHash { get; set; }
		public string PasswordSalt { get; set; }
		public string Name { get; set; }
		public DateTime? CreatedDate { get; set; }
		public DateTime? ModifiedDate { get; set; }
	}
}
