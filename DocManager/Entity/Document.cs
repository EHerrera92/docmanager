﻿using DocManager.Entity.Contracts;
using System;

namespace DocManager.Entity
{
	public class Document : IBaseEntity
	{
		public long Id { get; set; }
		public long UserId { get; set; }
		public DateTime PostedDate { get; set; }
		public DateTime? AccessedDate { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Category { get; set; }
		public byte[] Image { get; set; }
		public long[] Users { get; set; }
		public long[] Groups { get; set; }
		public DateTime? CreatedDate { get; set; }
		public DateTime? ModifiedDate { get; set; }
	}
}
