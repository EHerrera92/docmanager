﻿using DocManager.Entity.Contracts;
using System;

namespace DocManager.Entity
{
	public class Privilege : IBaseEntity
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public DateTime? CreatedDate { get; set; }
		public DateTime? ModifiedDate { get; set; }
	}
}
