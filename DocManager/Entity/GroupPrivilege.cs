﻿using DocManager.Entity.Contracts;
using System;

namespace DocManager.Entity
{
	public class GroupPrivilege : IBaseEntity
	{
		public long Id { get; set; }
		public long GroupId { get; set; }
		public long PrivilegeId { get; set; }
		public DateTime? CreatedDate { get; set; }
		public DateTime? ModifiedDate { get; set; }
	}
}
