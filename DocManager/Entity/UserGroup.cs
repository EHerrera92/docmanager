﻿using DocManager.Entity.Contracts;
using System;

namespace DocManager.Entity
{
	public class UserGroup : IBaseEntity
	{
		public long Id { get; set; }
		public long UserId { get; set; }		
		public long GroupId { get; set; }
		public DateTime? CreatedDate { get; set; }
		public DateTime? ModifiedDate { get; set; }
	}
}
