﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Domain
{
	public class Group : BaseDomain
	{
		public string Name { get; set; }
	}
}
