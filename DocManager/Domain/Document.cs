﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Domain
{
	public class Document : BaseDomain
	{
		public DateTime PostedDate { get; set; }
		public DateTime? AccessedDate { get; set; }
		public long UserId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Category { get; set; }
		public string Image { get; set; }
		public long[] Users { get; set; }
		public long[] Groups { get; set; }
	}
}
