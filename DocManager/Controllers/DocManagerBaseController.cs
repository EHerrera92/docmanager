﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Controllers
{
	[Route("api/[controller]")]
	public class DocManagerBaseController : Controller
	{
		protected long GetLoggedInUserId()
		{
			if(HttpContext != null && HttpContext.User != null)
			{
				return long.Parse(HttpContext.User.FindFirst(c => c.Type == "UserId").Value);
			}
			else
			{
				return 0;
			}
		}
		protected IActionResult MissingPrivilegeResponse()
		{
			return Forbid();
		}
	}
}
