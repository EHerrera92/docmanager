﻿using DocManager.Repositories.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Controllers
{
	public class PrivilegesController : DocManagerBaseController
	{
		private readonly IUserRepository _userRepo;
		private readonly IPrivilegeRepository _privilegeRepo;
		public PrivilegesController(IUserRepository userRepo, IPrivilegeRepository privilegeRepo)
		{
			_userRepo = userRepo;
			_privilegeRepo = privilegeRepo;
		}

		/// <summary>
		/// Retrieve a list of all privileges that a group can have
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[RequireHttps]
		[Authorize]
		public async Task<IActionResult> GetAll()
		{
			if(await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.ReadGroup))
			{
				var ents = await _privilegeRepo.GetAllAsync();
				List<Domain.Privilege> privileges = new List<Domain.Privilege>();
				foreach(var ent in ents)
				{
					privileges.Add(new Domain.Privilege {
						Id = ent.Id,
						Name = ent.Name
					});
				}
				return Ok(privileges);
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

	}
}
