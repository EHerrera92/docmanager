﻿using DocManager.Entity;
using DocManager.Repositories.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Controllers
{
	public class GroupsController : DocManagerBaseController
	{
		private readonly IGroupRepository _groupRepo;
		private readonly IUserRepository _userRepo;
		private readonly IPrivilegeRepository _privilegeRepo;
		public GroupsController(IGroupRepository groupRepo, IUserRepository userRepo, IPrivilegeRepository privilegeRepo)
		{
			_groupRepo = groupRepo;
			_userRepo = userRepo;
			_privilegeRepo = privilegeRepo;
		}

		/// <summary>
		/// Retrieve a list of all groups
		/// </summary>
		/// <returns>Array of Group</returns>
		[HttpGet]
		[RequireHttps]
		[Authorize]
		public async Task<IActionResult> GetAllGroups()
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.ReadGroup))
			{
				List<Domain.Group> groups = new List<Domain.Group>();
				var ents = await _groupRepo.GetAllAsync();
				foreach (var ent in ents)
				{
					groups.Add(new Domain.Group
					{
						Id = ent.Id,
						Name = ent.Name
					});
				}
				return Ok(groups);
			}
			else
				return MissingPrivilegeResponse();
		}

		/// <summary>
		/// Retrieve a specific group
		/// </summary>
		/// <param name="groupId">The Id of the group to retrieve</param>
		/// <returns>Group</returns>
		[HttpGet]
		[RequireHttps]
		[Authorize]
		[Route("{groupId}")]
		public async Task<IActionResult> GetOneGroup(long groupId)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.ReadGroup))
			{
				var groupEnt = await _groupRepo.GetAsync(groupId);
				if (groupEnt == null)
				{
					return NotFound();
				}
				else
				{
					return Ok(groupEnt);
				}
			}
				
			else
				return MissingPrivilegeResponse();
		}

		/// <summary>
		/// Remove a specific group
		/// </summary>
		/// <param name="id">The Id of the group to delete</param>
		/// <returns></returns>
		[HttpDelete]
		[RequireHttps]
		[Authorize]
		[Route("{id}")]
		public async Task<IActionResult> DeleteOneGroup(long id)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.DeleteGroup))
			{
				var entToDelete = await _groupRepo.GetAsync(id);
				if (entToDelete == null)
				{
					return NotFound();
				}
				else
				{
					await _groupRepo.DeleteAsync(entToDelete);
					return Ok();
				}
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Add a new group if one with the same name doesn't already exist
		/// </summary>
		/// <param name="groupRequest">Group object to add</param>
		/// <returns></returns>
		[HttpPost]
		[RequireHttps]
		[Authorize]
		public async Task<IActionResult> AddOneGroup([FromBody] Domain.Group groupRequest)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.CreateGroup))
			{
				//Validate new group
				if (groupRequest.Name == null || groupRequest.Name.Length == 0)
				{
					return BadRequest("Name is required.");
				}
				var existingEnt = await _groupRepo.GetByNameAsync(groupRequest.Name);
				if (existingEnt != null)
				{
					return BadRequest("A group with this name already exists.");
				}

				Group newEnt = new Group
				{
					Name = groupRequest.Name
				};
				newEnt = await _groupRepo.InsertAsync(newEnt);
				groupRequest.Id = newEnt.Id;
				return CreatedAtAction("GetOneGroup", "Groups", new { groupId = newEnt.Id }, groupRequest);
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Add a privilege to a group if it doesn't have it already
		/// </summary>
		/// <param name="groupId">The group to add the privilege to</param>
		/// <returns></returns>
		[HttpPost]
		[RequireHttps]
		[Authorize]
		[Route("{groupId}/privileges")]
		public async Task<IActionResult> AddPrivilegeToGroup([FromBody] Domain.Privilege privilegeRequest, long groupId)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.AddPrivilegeToGroup))
			{
				if (privilegeRequest.Id == 0)
					return BadRequest("Privilege Id is missing.");
				var privilegeToAdd = await _privilegeRepo.GetAsync(privilegeRequest.Id);
				if (privilegeToAdd == null)
					return BadRequest($"Privilege does not exist.");
				var existingPrivileges = await _groupRepo.GetPrivilegesAsync(groupId);
				if (existingPrivileges.Where(p => p.Id == privilegeToAdd.Id).Any())
					return BadRequest("Group already has this privilege.");

				await _groupRepo.AddPrivilegeAsync(groupId, privilegeRequest.Id);
				return Ok();
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Remove privilege from group if it has it
		/// </summary>
		/// <param name="groupId">Group from which to remove the privilege</param>
		/// <param name="privilegeId">Privilege to remove</param>
		/// <returns></returns>
		[HttpDelete]
		[RequireHttps]
		[Authorize]
		[Route("{groupId}/privileges/{privilegeId}")]
		public async Task<IActionResult> RemovePrivilegeFromGroup(long groupId, long privilegeId)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.RemovePrivilegeFromGroup))
			{
				if ((await _groupRepo.GetAsync(groupId)) == null)
					return BadRequest("Group does not exist.");
				var gp = (await _groupRepo.GetPrivilegesAsync(groupId)).Where(se => se.Id == privilegeId);
				if (!gp.Any())
					return BadRequest("Group does not have this privilege.");
				await _groupRepo.RemovePrivilegeAsync(groupId, privilegeId);
				return Ok();
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// List privileges for given group
		/// </summary>
		/// <param name="groupId">Group Id</param>
		/// <returns>List of priviliges</returns>
		[HttpGet]
		[RequireHttps]
		[Authorize]
		[Route("{groupId}/privileges")]
		public async Task<IActionResult> GetPrivileges(long groupId)
		{
			if(await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.ReadGroup))
			{
				var group = _groupRepo.GetAsync(groupId);
				if (group == null)
					return BadRequest("Group does not exist.");
				List<Domain.Privilege> groupPriviliges = new List<Domain.Privilege>();
				var ents = await _groupRepo.GetPrivilegesAsync(groupId);
				foreach(var ent in ents)
				{
					groupPriviliges.Add(new Domain.Privilege {
						Id = ent.Id,
						Name = ent.Name
					});
				}
				return Ok(groupPriviliges);
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Update a group if it exists
		/// </summary>
		/// <param name="group">Updated group data</param>
		/// <param name="groupId">Group Id to update</param>
		/// <returns></returns>
		[HttpPut]
		[RequireHttps]
		[Authorize]
		[Route("{groupId}")]
		public async Task<IActionResult> Put([FromBody] Domain.Group group, long groupId)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.UpdateGroup))
			{
				if (group.Id == 0)
					return BadRequest("Group does not exist.");
				var groupToEdit = await _groupRepo.GetAsync(groupId);
				if (groupToEdit == null)
					return NotFound($"Group does not exist.");
				Group ent = new Group {
					Id = groupId,
					Name = group.Name,
				};
				await _groupRepo.UpdateAsync(ent);
				return Ok();
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}
	}
}
