﻿using DocManager.Repositories.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Controllers
{
	public class DocumentsController : DocManagerBaseController
	{
		private readonly IUserRepository _userRepo;
		private readonly IDocumentRepository _docRepo;
		public DocumentsController(IUserRepository userRepo, IDocumentRepository docRepo)
		{
			_userRepo = userRepo;
			_docRepo = docRepo;
		}

		[HttpPost]
		[RequireHttps]
		[Authorize]
		public async Task<IActionResult> UploadDocument([FromBody] Domain.Document request)
		{
			if(await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.UploadDocument))
			{
				if (request.Image == null || request.Name == null || request.Name.Length == 0)
					return BadRequest("Image and Name are mandatory fields.");
				var ent = await _docRepo.InsertAsync(new Entity.Document
				{
					UserId = GetLoggedInUserId(),
					PostedDate = DateTime.Now,
					AccessedDate = null,
					Name = request.Name,
					Category = request.Category,
					Description = request.Description,
					Image = Convert.FromBase64String(request.Image),
					Users = request.Users,
					Groups = request.Groups
				});
				request.Id = ent.Id;
				request.Image = null;
				return CreatedAtAction("DownloadDocument", "Documents", new { documentId = ent.Id }, request);
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Retrieve a document's metadata and content. Caller must satisfy one of these conditions:
		/// - Owner of document
		/// - Explicitly given access to the document
		/// - Member of a group explicitly given access to the document
		/// - Member of a group with the "View Other User Documents" privilege
		/// </summary>
		/// <param name="documentId"></param>
		/// <returns></returns>
		[HttpGet] 
		[RequireHttps]
		[Authorize]
		[Route("{documentId}")]
		public async Task<IActionResult> DownloadDocument(long documentId)
		{
			if(await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.DownloadDocument)) {
				var doc = await _docRepo.GetAsync(documentId);
				if(doc == null)
				{
					return NotFound();
				}
				else
				{
					var hasAccess = false;
					if (doc.UserId == GetLoggedInUserId() || (doc.Users != null && doc.Users.Contains(GetLoggedInUserId())) || await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.ViewOtherUserDocuments))
					{
						hasAccess = true;
					}
					else if(doc.Groups != null)
					{
						var groups = await _userRepo.GetGroupsAsync(GetLoggedInUserId());
						var myGroupIds = groups.Select(g => g.Id).ToArray();
						hasAccess = doc.Groups.Select(x => x).Intersect(myGroupIds).Any();
					}

					if(hasAccess)
					{
						doc.AccessedDate = DateTime.Now;
						await _docRepo.UpdateAsync(doc);
						return Ok(new Domain.Document
						{
							Id = doc.Id,
							UserId = doc.UserId,
							Description = doc.Description,
							AccessedDate = doc.AccessedDate,
							PostedDate = doc.PostedDate,
							Category = doc.Category,
							Image = Convert.ToBase64String(doc.Image),
							Name = doc.Name
						});
					}
					else
					{
						return MissingPrivilegeResponse();
					}
				}
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}
	}
	public class DocumentModel
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public string Category { get; set; }
		public string Image { get; set; }
	}
}
