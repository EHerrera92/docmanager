﻿using DocManager.Entity;
using DocManager.Domain;
using DocManager.Repositories.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocManager.Common;
using System.Text.RegularExpressions;

namespace DocManager.Controllers
{
	public class UsersController : DocManagerBaseController
	{
		private readonly IUserRepository _userRepo;
		private readonly IGroupRepository _groupRepo;

		public UsersController(IUserRepository userRepo, IGroupRepository groupRepo)
		{
			_userRepo = userRepo;
			_groupRepo = groupRepo;
		}

		/// <summary>
		/// Retrieve all users
		/// </summary>
		/// <returns>Users</returns>
		[HttpGet]
		[RequireHttps]
		[Authorize]
		public async Task<IActionResult> GetAllUsers()
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.ReadUser))
			{
				var ents = await _userRepo.GetAllAsync();
				List<Domain.User> users = new List<Domain.User>();
				foreach(var ent in ents)
				{
					users.Add(new Domain.User {
						Id = ent.Id,
						Name = ent.Name,
						Username = ent.Username
					});
				}
				return Ok(users);
			}
			else
				return MissingPrivilegeResponse();
		}


		/// <summary>
		/// Retrieve a specific user
		/// </summary>
		/// <param name="userId">Id of the user to retrieve</param>
		/// <returns></returns>
		[HttpGet]
		[RequireHttps]
		[Authorize]
		[Route("{userId}")]
		public async Task<IActionResult> GetOneUser(long userId)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.ReadUser))
			{
				var userEnt = await _userRepo.GetAsync(userId);
				if(userEnt == null)
				{
					return NotFound();
				}
				else
				{
					return Ok(new Domain.User {
						Id = userEnt.Id,
						Name = userEnt.Name,
						Username = userEnt.Username,
						Password = null
					});
				}
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}


		/// <summary>
		/// Delete a specific user
		/// </summary>
		/// <param name="userId">Id of the user to delete</param>
		/// <returns></returns>
		[HttpDelete]
		[RequireHttps]
		[Authorize]
		[Route("{userId}")]
		public async Task<IActionResult> DeleteOneUser(long userId)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.DeleteUser))
			{
				var userEnt = _userRepo.GetAsync(userId);
				if(userEnt == null)
				{
					return NotFound();
				}
				else
				{
					await _userRepo.DeleteAsync(userId);
					return Ok();
				}
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Add a user
		/// </summary>
		/// <param name="user">User data</param>
		/// <returns>New user</returns>
		[HttpPost]
		[RequireHttps]
		[Authorize]
		public async Task<IActionResult> AddOneUser([FromBody] Domain.User user)
		{
			long userId = GetLoggedInUserId();
			if (await _userRepo.HasPrivilegeAsync(userId, IUserRepository.PrivilegeType.CreateUser))
			{
				var oldEnt = await _userRepo.GetByUsernameAsync(user.Username);
				if(oldEnt != null)
					return BadRequest("A user with this username already exists.");

				if (user.Username == null || user.Username.Length < 3 || user.Username.Length > 50)
					return BadRequest("Username must be between 3 and 50 characters long.");

				if (user.Password == null || user.Password.Length < 6 || user.Password.Length > 50)
					return BadRequest("Password must be between 6 and 50 characters long.");

				if (user.Name == null || user.Name.Length < 2 || user.Name.Length > 100)
					return BadRequest("Name must be between 2 and 100 characters long.");

				if (!new Regex("^[a-zA-Z0-9]+$").IsMatch(user.Username))
					return BadRequest("Name can only contain alphanumeric characters");

				var salt = AuthUtils.ComputeSalt();
				var newEnt = await _userRepo.InsertAsync(new Entity.User {
					Username = user.Username.ToLower(),
					Name = user.Name,
					PasswordHash = AuthUtils.ComputeHash(user.Password, salt),
					PasswordSalt = salt
				});
				user.Id = newEnt.Id;
				return CreatedAtAction("GetOneUser", "Users", new { userId = newEnt.Id }, user);
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Give the user membership to a new group
		/// </summary>
		/// <param name="group"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		[HttpPost]
		[RequireHttps]
		[Authorize]
		[Route("{userId}/groups")]
		public async Task<IActionResult> AddUserToGroup([FromBody] Domain.Group group, long userId)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.AddUserToGroup))
			{
				if( group.Id == 0)
					return BadRequest("Group Id is missing.");

				if (!await _groupRepo.ExistsAsync(group.Id))
					return BadRequest($"Group does not exist.");

				var userEnt = await _userRepo.GetAsync(userId);
				if (!await _userRepo.ExistsAsync(userId))
					return BadRequest($"User does not exist.");

				if ((await _userRepo.GetGroupsAsync(userId)).Where(g => g.Id == group.Id).Any())
					return BadRequest("User is already a member of this group.");

				Entity.UserGroup newEnt = new Entity.UserGroup
				{
					GroupId = group.Id,
					UserId = userId
				};
				await _userRepo.AddToGroupAsync(userId, group.Id);
				return Ok();
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Removes user from group
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="groupId"></param>
		/// <returns></returns>
		[HttpDelete]
		[RequireHttps]
		[Authorize]
		[Route("{userId}/groups/{groupId}")]
		public async Task<IActionResult> RemoveUserFromGroup(long userId, long groupId)
		{
			long loggedInUserId = GetLoggedInUserId();
			if (await _userRepo.HasPrivilegeAsync(loggedInUserId, IUserRepository.PrivilegeType.RemoveUserFromGroup))
			{
				if (! await _userRepo.ExistsAsync(userId))
					return BadRequest("User does not exist.");
				var groupEnts = await _userRepo.GetGroupsAsync(userId);
				if(!groupEnts.Where(ug => ug.Id == groupId).Any())
					return BadRequest("User is not a member of this group.");
				
				await _userRepo.RemoveFromGroupAsync(userId, groupId);
				return Ok();
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Get a list of groups the user is a member of
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		[HttpGet]
		[RequireHttps]
		[Authorize]
		[Route("{userId}/groups")]
		public async Task<IActionResult> GetUserGroups(long userId)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.ReadGroup))
			{
				if (!await _userRepo.ExistsAsync(userId))
					return BadRequest("User does not exist.");

				List<Domain.Group> groups = new List<Domain.Group>();
				var ents = await _userRepo.GetGroupsAsync(userId);
				foreach(var ent in ents)
				{
					groups.Add(new Domain.Group {
						Id = ent.Id,
						Name = ent.Name
					});
				}
				return Ok(groups);
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}

		/// <summary>
		/// Update a user if it exists
		/// </summary>
		/// <param name="user">Updated group data</param>
		/// <param name="userId">Group Id to update</param>
		/// <returns></returns>
		[HttpPut]
		[RequireHttps]
		[Authorize]
		[Route("{userId}")]
		public async Task<IActionResult> UpdateUser([FromBody] Domain.User user, long userId)
		{
			if (await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.UpdateUser))
			{
				var oldEnt = await _userRepo.GetAsync(userId);
				if(oldEnt == null)
				{
					return BadRequest("User does not exist.");
				}
				else
				{
					if (user.Username == null || user.Username.Length < 3 || user.Username.Length > 50)
						return BadRequest("Username must be between 3 and 50 characters long.");
					if (user.Name.Length < 2 || user.Name.Length > 100)
						return BadRequest("Name must be between 2 and 100 characters long.");
					if (user.Password != null && (user.Password.Length < 6 || user.Password.Length > 50))
						return BadRequest("Password must be between 6 and 50 characters long.");
					if (!new Regex("^[a-zA-Z0-9]+$").IsMatch(user.Username))
						return BadRequest("Name can only contain alphanumeric characters");

					oldEnt.Username = user.Username.ToLower();
					oldEnt.Name = user.Name;
					
					if(user.Password != null)
					{
						string salt = AuthUtils.ComputeSalt();
						oldEnt.PasswordSalt = salt;
						oldEnt.PasswordHash = AuthUtils.ComputeHash(user.Password, salt);
					}
					await _userRepo.UpdateAsync(oldEnt);
					return Ok();
				}
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}


		/// <summary>
		/// View documents created by given user. Caller must meet one of these criteria: 
		/// - Owner of document
		/// - Member of group with "View Other User Documents" privilege
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		[HttpGet]
		[RequireHttps]
		[Authorize]
		[Route("{userId}/documents")]
		public async Task<IActionResult> GetDocuments(long userId)
		{
			if(userId == GetLoggedInUserId() || await _userRepo.HasPrivilegeAsync(GetLoggedInUserId(), IUserRepository.PrivilegeType.ViewOtherUserDocuments))
			{
				var ents = await _userRepo.GetDocuments(userId);
				List<Domain.Document> docs = new List<Domain.Document>();
				foreach (var ent in ents)
				{
					docs.Add(new Domain.Document
					{
						Id = ent.Id,
						UserId = ent.UserId,
						Description = ent.Description,
						AccessedDate = ent.AccessedDate,
						PostedDate = ent.PostedDate,
						Category = ent.Category,
						Users = ent.Users,
						Groups = ent.Groups,
						Name = ent.Name
					});
				}
				return Ok(docs);
			}
			else
			{
				return MissingPrivilegeResponse();
			}
		}
	}
}
