﻿/*
 * Used for centralized error handling 
 * 
 */
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Controllers
{
	public class ErrorController : DocManagerBaseController
	{
		ILogger _logger;
		public ErrorController(ILogger<ErrorController> logger)
		{
			_logger = logger;
		}

		[HttpGet]
		public IActionResult OnError()
		{
			var errorInfo = this.HttpContext.Features.Get<IExceptionHandlerFeature>();
			_logger.LogError(errorInfo.Error.ToString());
			return StatusCode(500);
		}
	}
}
