﻿using DocManager.Common;
using DocManager.Entity;
using DocManager.Repositories.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DocManager.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private IConfiguration _config;
        private IUserRepository _userRepo;

        public TokenController(IConfiguration config, IUserRepository userRepo)
        {
            _config = config;
            _userRepo = userRepo;
        }

        /// <summary>
        /// Create a token from username and password. This token needs to be part of the Authorization header of every request
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [RequireHttps]
        public async Task<IActionResult> CreateToken([FromBody] LoginModel login)
        {
            IActionResult response = Unauthorized();
            var user = await _userRepo.GetByUsernameAsync(login.Username);
            if (user != null)
			{
                if (AuthUtils.ComputeHash(login.Password, user.PasswordSalt) == user.PasswordHash)
                {
                    var tokenString = BuildToken(user);
                    response = Ok(new { token = tokenString });
                }
            }
            return response;
        }

        private string BuildToken(User user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim("UserId", user.Id.ToString())
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(_config.GetValue<int>("Jwt:LifespanInMinutes")),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public class LoginModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

    }
}