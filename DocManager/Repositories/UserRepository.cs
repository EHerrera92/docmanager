﻿using DocManager.Entity;
using DocManager.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace DocManager.Repositories
{
	public class UserRepository : BaseRepository<User>, IUserRepository
	{
		public UserRepository(IConfiguration cfg) : base(cfg)
		{

		}

		public async Task<User> GetByUsernameAsync(string username)
		{
			username = username.ToLower();
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = "docmanager.\"user_get_by_username\"";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_inusername", username);
			NpgsqlDataReader r = await cmd.ExecuteReaderAsync();
			var user = await DbRecToEntity<User>(r);
			return user;
		}

		public async Task<bool> HasPrivilegeAsync(long id, IUserRepository.PrivilegeType perm)
		{
			var conn = new NpgsqlConnection(_cs);
			try
			{
				if (conn.State != ConnectionState.Open)
					await conn.OpenAsync();
				using var cmd = new NpgsqlCommand("call docmanager.\"USER_HAS_PRIVILEGE\"(@_InUserId,@_InPrivilegeId,@_OutResult)", (NpgsqlConnection)conn);
				cmd.Parameters.AddWithValue("_InUserId", id);
				cmd.Parameters.AddWithValue("_InPrivilegeId", (long)perm);
				cmd.Parameters.Add(new NpgsqlParameter("_OutResult", DbType.Int32) { Direction = ParameterDirection.InputOutput, Value = 0 });
				await cmd.ExecuteNonQueryAsync();
				return (int)cmd.Parameters[2].Value > 0;
			}
			finally
			{
				if (conn != null && conn.State == ConnectionState.Open)
					await conn.CloseAsync();
			}
		}

		public async Task<IEnumerable<Group>> GetGroupsAsync(long userId)
		{
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = "docmanager.\"user_get_groups\"";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_inuserid", userId);
			NpgsqlDataReader r = await cmd.ExecuteReaderAsync();
			var groups = await DbRecsToEntities<Group>(r);
			return groups;
		}

		public async Task AddToGroupAsync(long userId, long groupId)
		{
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			List<Group> groups = new List<Group>();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = "docmanager.\"user_add_to_group\"";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_inuserid", userId);
			cmd.Parameters.AddWithValue("_ingroupid", groupId);
			await cmd.ExecuteNonQueryAsync();
		}

		public async Task RemoveFromGroupAsync(long userId, long groupId)
		{
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			List<Group> groups = new List<Group>();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = "docmanager.\"user_remove_from_group\"";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_inuserid", userId);
			cmd.Parameters.AddWithValue("_ingroupid", groupId);
			await cmd.ExecuteNonQueryAsync();
		}

		public async Task<IEnumerable<Document>> GetDocuments(long userId)
		{
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = "docmanager.\"user_get_documents\"";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_inuserid", userId);
			NpgsqlDataReader r = await cmd.ExecuteReaderAsync();
			var documents = await DbRecsToEntities<Document>(r);
			return documents;
		}
	}
}
