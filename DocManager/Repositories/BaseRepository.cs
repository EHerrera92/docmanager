﻿using DocManager.Entity.Contracts;
using DocManager.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace DocManager.Repositories
{
	public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IBaseEntity, new()
	{
		protected string _cs;

		public BaseRepository(IConfiguration cfg) 
		{
			_cs = cfg.GetConnectionString("Postgres");
		}
		public async Task<TEntity> InsertAsync(TEntity ent)
		{
			string spName = $"docmanager.\"{typeof(TEntity).Name.ToLower()}_insert\"";
			ent.CreatedDate = DateTime.Now;
			ent.ModifiedDate = DateTime.Now;

			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			List<TEntity> users = new List<TEntity>();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = spName;
			cmd.CommandType = CommandType.StoredProcedure;
			foreach(var property in typeof(TEntity).GetProperties())
			{
				if (property.Name == "Id")
					continue;
				var value = property.GetValue(ent, null);
				if(value == null)
					cmd.Parameters.AddWithValue($"_{property.Name.ToLower()}", DBNull.Value);
				else
					cmd.Parameters.AddWithValue($"_{property.Name.ToLower()}", value);
			}
			long newId = (long)(await cmd.ExecuteScalarAsync());
			ent.Id = newId;
			return ent;
		}
		public async Task DeleteAsync(TEntity ent)
		{
			await this.DeleteAsync(ent.Id);
		}

		public async Task DeleteAsync(long id)
		{
			string spName = $"docmanager.\"{typeof(TEntity).Name.ToLower()}_delete\"";
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = spName;
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_id", id);
			await cmd.ExecuteNonQueryAsync();
		}

		public async Task<TEntity> GetAsync(long id)
		{
			string spName = $"docmanager.\"{typeof(TEntity).Name.ToLower()}_get\"";
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = spName;
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_id", id);
			NpgsqlDataReader r = await cmd.ExecuteReaderAsync();
			var ent = await DbRecToEntity<TEntity>(r);
			return ent;
		}

		protected async Task<IEnumerable<T>> DbRecsToEntities<T>(NpgsqlDataReader reader) where T : new()
		{
			Dictionary<string, string> propertyNameToColName = new Dictionary<string, string>();
			var properties = typeof(T).GetProperties();
			foreach(var property in properties)
			{
				propertyNameToColName.Add(property.Name, CamelCaseToSnakeCase(property.Name));
			}
			List<T> ents = new List<T>();
			while (await reader.ReadAsync())
			{
				T ent = new T();
				foreach (var property in properties)
				{
					var rowValue = reader.GetValue(propertyNameToColName[property.Name]);
					if (rowValue is DBNull)
						property.SetValue(ent, null);
					else
						property.SetValue(ent, rowValue);
				}
				ents.Add(ent);
			}
			return ents;
		}
		
		protected async Task<T> DbRecToEntity<T>(NpgsqlDataReader reader) where T : new()
		{
			var properties = typeof(T).GetProperties();
			T ent = default;
			while (await reader.ReadAsync())
			{
				ent = new T();
				foreach (var property in properties)
				{
					var rowValue = reader.GetValue(CamelCaseToSnakeCase(property.Name));
					if (rowValue is DBNull)
						property.SetValue(ent, null);
					else
						property.SetValue(ent, rowValue);
				}
				break;
			}
			return ent;
		}


		public async Task<IEnumerable<TEntity>> GetAllAsync()
		{
			string spName = $"docmanager.\"{typeof(TEntity).Name.ToLower()}_get_all\"";
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = spName;
			cmd.CommandType = CommandType.StoredProcedure;
			NpgsqlDataReader r = await cmd.ExecuteReaderAsync();
			var ents = await DbRecsToEntities<TEntity>(r);
			return ents;
		}

		public async Task UpdateAsync(TEntity ent)
		{
			string spName = $"docmanager.\"{typeof(TEntity).Name.ToLower()}_update\"";
			ent.ModifiedDate = DateTime.Now;

			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = spName;
			cmd.CommandType = CommandType.StoredProcedure;

			foreach (var property in typeof(TEntity).GetProperties())
			{
				var value = property.GetValue(ent, null);
				if(value == null)
					cmd.Parameters.AddWithValue($"_{property.Name.ToLower()}", DBNull.Value);
				else
					cmd.Parameters.AddWithValue($"_{property.Name.ToLower()}", value);
			}
			await cmd.ExecuteNonQueryAsync();
		}

		private string CamelCaseToSnakeCase(string s)
		{
			char[] ch = s.ToCharArray();
			StringBuilder sb = new StringBuilder();
			sb.Append(Char.ToLower(ch[0]));
			for (var i = 1; i < ch.Length; i++)
			{
				if (Char.IsUpper(ch[i]))
					sb.Append('_');
				sb.Append(Char.ToLower(ch[i]));
			}
			return sb.ToString();
		}

		public async Task<bool> ExistsAsync(long id)
		{
			bool exists = (await GetAsync(id)) != null;
			return exists;
		}
	}
}
