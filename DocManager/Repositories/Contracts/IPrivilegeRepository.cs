﻿using DocManager.Entity;
using System;

namespace DocManager.Repositories.Contracts
{
	public interface IPrivilegeRepository : IBaseRepository<Privilege>
	{
	}
}
