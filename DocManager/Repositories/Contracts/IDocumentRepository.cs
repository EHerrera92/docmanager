﻿using DocManager.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Repositories.Contracts
{
	public interface IDocumentRepository : IBaseRepository<Document>
	{
		
	}
}
