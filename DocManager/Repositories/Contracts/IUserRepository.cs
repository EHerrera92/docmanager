﻿using DocManager.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DocManager.Repositories.Contracts
{
	public interface IUserRepository : IBaseRepository<User>
	{
		//These must match Ids in database table Privilege
		public enum PrivilegeType
		{
			DownloadDocument = 1,
			UploadDocument = 2,
			CreateUser = 3,
			ReadUser = 4,
			UpdateUser = 5,
			DeleteUser = 6,
			CreateGroup = 7,
			ReadGroup = 8,
			UpdateGroup = 9,
			DeleteGroup = 10,
			AddUserToGroup = 11,
			RemoveUserFromGroup = 12,
			AddPrivilegeToGroup = 13,
			RemovePrivilegeFromGroup = 14,
			ViewOtherUserDocuments = 15
		};
		public Task<bool> HasPrivilegeAsync(long userId, PrivilegeType perm);
		public Task<User> GetByUsernameAsync(string username);
		public Task<IEnumerable<Group>> GetGroupsAsync(long userId);
		public Task AddToGroupAsync(long userId, long groupId);
		public Task RemoveFromGroupAsync(long userId, long groupId);
		public Task<IEnumerable<Document>> GetDocuments(long userId);
	}
}
