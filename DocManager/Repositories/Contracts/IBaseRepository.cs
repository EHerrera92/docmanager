﻿using DocManager.Entity.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DocManager.Repositories.Contracts
{
	public interface IBaseRepository<TEntity> where TEntity : class, IBaseEntity
	{
		public Task<IEnumerable<TEntity>> GetAllAsync();
		public Task<TEntity> InsertAsync(TEntity ent);
		public Task<bool> ExistsAsync(long id);
		public Task DeleteAsync(TEntity ent);
		public Task UpdateAsync(TEntity ent);
		public Task DeleteAsync(long id);
		public Task<TEntity> GetAsync(long id);
	}
}
