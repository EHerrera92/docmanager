﻿using DocManager.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Repositories.Contracts
{
	public interface IGroupRepository : IBaseRepository<Group>
	{
		Task<Group> GetByNameAsync(string name);
		Task<IEnumerable<Privilege>> GetPrivilegesAsync(long groupId);
		Task AddPrivilegeAsync(long groupId, long privilegeId);
		Task RemovePrivilegeAsync(long groupId, long privilegeId);
	}
}
