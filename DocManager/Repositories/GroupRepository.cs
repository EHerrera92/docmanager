﻿using DocManager.Entity;
using DocManager.Repositories.Contracts;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DocManager.Repositories
{
	public class GroupRepository : BaseRepository<Group>, IGroupRepository
	{
		public GroupRepository(IConfiguration cfg) : base(cfg)
		{
			
		}

		public async Task<Group> GetByNameAsync(string name)
		{
			name = name.ToLower();
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = "docmanager.\"group_get_by_name\"";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_inname", name);
			NpgsqlDataReader r = await cmd.ExecuteReaderAsync();
			var rec = await DbRecToEntity<Group>(r);
			return rec;
		}

		public async Task<IEnumerable<Privilege>> GetPrivilegesAsync(long groupId)
		{
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			List<Document> docs = new List<Document>();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = "docmanager.\"group_get_privileges\"";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_ingroupid", groupId);
			NpgsqlDataReader r = await cmd.ExecuteReaderAsync();
			var privileges = await this.DbRecsToEntities<Privilege>(r);
			return privileges;
		}

		public async Task AddPrivilegeAsync(long groupId, long privilegeId)
		{
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = "docmanager.\"group_add_privilege\"";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_ingroupid", groupId);
			cmd.Parameters.AddWithValue("_inprivilegeid", privilegeId);
			await cmd.ExecuteNonQueryAsync();
		}

		public async Task RemovePrivilegeAsync(long groupId, long privilegeId)
		{
			using var conn = new NpgsqlConnection(_cs);
			conn.Open();
			using NpgsqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = "docmanager.\"group_remove_privilege\"";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("_ingroupid", groupId);
			cmd.Parameters.AddWithValue("_inprivilegeid", privilegeId);
			await cmd.ExecuteNonQueryAsync();
		}
	}
}
