﻿using DocManager.Entity;
using DocManager.Repositories.Contracts;
using Microsoft.Extensions.Configuration;
using System;

namespace DocManager.Repositories
{
	public class DocumentRepository : BaseRepository<Document>, IDocumentRepository
	{
		public DocumentRepository(IConfiguration cfg) : base(cfg)
		{

		}
	}
}
