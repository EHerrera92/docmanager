﻿using DocManager.Entity;
using DocManager.Repositories.Contracts;
using Microsoft.Extensions.Configuration;
using System;

namespace DocManager.Repositories
{
	public class PrivilegeRepository : BaseRepository<Privilege>, IPrivilegeRepository
	{
		public PrivilegeRepository(IConfiguration cfg) : base(cfg)
		{

		}
	}
}
