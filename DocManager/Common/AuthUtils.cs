﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DocManager.Common
{
	public class AuthUtils
	{
        public static string ComputeSalt()
        {
            byte[] salt = new byte[64];
            Random r = new Random();
            for (int i = 0; i < 64; i++)
            {
                salt[i] = (byte)r.Next(0, 256);
            }
            return BitConverter.ToString(salt).Replace("-", "");
        }
        public static string ComputeHash(string plainText, string salt)
        {
            using (var hasher = SHA512.Create())
            {
                return BitConverter.ToString(hasher.ComputeHash(Encoding.UTF8.GetBytes($"{salt}{plainText}"))).Replace("-", "");
            }
        }
    }
}
