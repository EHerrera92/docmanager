﻿using DocManager.Domain;
using DocManager.Repositories.Contracts;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static DocManager.Controllers.TokenController;

namespace DocManagerTestE2E
{
	class Program
	{
		private static Random r = new Random();
		static async Task Main(string[] args)
		{
			await Task.Delay(1000);
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri("https://localhost:44385/api/");
			var tokenRes = await client.PostAsync("Token", new StringContent("{\"Username\": \"admin\", \"Password\": \"password\"}", Encoding.UTF8, "application/json"));
			dynamic token = JsonConvert.DeserializeObject<dynamic>(await tokenRes.Content.ReadAsStringAsync());
			string adminToken = token.token;
			Console.WriteLine($"Admin token is: {adminToken}");

			//Add manager user
			User manager = new User
			{
				Username = RandomString(8),
				Name = "Manager " + RandomString(8),
				Password = "password"
			};

			HttpClient adminClient = new HttpClient();
			adminClient.BaseAddress = new Uri("https://localhost:44385/api/");
			adminClient.DefaultRequestHeaders.Add("Authorization", $"bearer {adminToken}");
			//Try to create manager without using bearer token
			var withoutTokenManagerResult = await client.PostAsync("users", new StringContent(JsonConvert.SerializeObject(manager), Encoding.UTF8, "application/json"));
			if (withoutTokenManagerResult.StatusCode != System.Net.HttpStatusCode.Unauthorized)
				throw new Exception("Manager was created without needing a token");

	
			var withTokenManagerResult = await adminClient.PostAsync("Users", new StringContent(JsonConvert.SerializeObject(manager), Encoding.UTF8, "application/json"));
			manager = JsonConvert.DeserializeObject<User>(await withTokenManagerResult.Content.ReadAsStringAsync());
			Console.WriteLine($"Created manager user {manager.Username} with id {manager.Id} and password {manager.Password}");

			tokenRes = await client.PostAsync("Token", new StringContent(JsonConvert.SerializeObject(new LoginModel { Username = manager.Username, Password = manager.Password }), Encoding.UTF8, "application/json"));
			token = JsonConvert.DeserializeObject<dynamic>(await tokenRes.Content.ReadAsStringAsync());
			string managerToken = token.token;
			Console.WriteLine($"Manager token is {managerToken}");

			HttpClient managerClient = new HttpClient();
			managerClient.BaseAddress = new Uri("https://localhost:44385/api/");
			managerClient.DefaultRequestHeaders.Add("Authorization", $"bearer {managerToken}");

			//Add manager to manager group
			await adminClient.PostAsync($"users/{manager.Id}/groups", new StringContent(JsonConvert.SerializeObject(new Group { Id = 2L }), Encoding.UTF8, "application/json"));

			//Add user
			User user = new User
			{
				Username = RandomString(8),
				Name = "User " + RandomString(8),
				Password = "password"
			};
			var userCreationResult = await adminClient.PostAsync("Users", new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json"));
			user = JsonConvert.DeserializeObject<User>(await userCreationResult.Content.ReadAsStringAsync());
			Console.WriteLine($"Created regular user {user.Username} with id {user.Id} and password {user.Password}");

			tokenRes = await client.PostAsync("Token", new StringContent(JsonConvert.SerializeObject(new LoginModel { Username = user.Username, Password = user.Password }), Encoding.UTF8, "application/json"));
			token = JsonConvert.DeserializeObject<dynamic>(await tokenRes.Content.ReadAsStringAsync());
			string userToken = token.token;
			Console.WriteLine($"User token is {userToken}");

			HttpClient userClient = new HttpClient();
			userClient.BaseAddress = new Uri("https://localhost:44385/api/");
			userClient.DefaultRequestHeaders.Add("Authorization", $"bearer {userToken}");

			//Expect user to see manager doc due to explicitly access granted.
			//Expect manger to see manager doc due to ownership
			//Expect admin to see manager doc due to group privilege.
			//At first user is missing grou prpivielge to download docs, so won't be able to download until after being added to user's group.
			var managerDoc = new Document
			{
				Name = "manager.png",
				Description = "Manager's doc",
				Category = "Testing",
				Image = GetDocument(),
				Users = new long[] { user.Id }
			};

			var managerCreateDocresult = await managerClient.PostAsync("Documents", new StringContent(JsonConvert.SerializeObject(managerDoc), Encoding.UTF8, "application/json"));
			managerDoc = JsonConvert.DeserializeObject<Document>(await managerCreateDocresult.Content.ReadAsStringAsync());
			Console.WriteLine($"Created manager doc with id {managerDoc.Id}");

			var userGetManagerDocResult = await userClient.GetAsync($"documents/{managerDoc.Id}");
			Console.WriteLine($"Status for userGetManagerDocResult was {userGetManagerDocResult.StatusCode}. Expected Forbidden");

			var managerGetManagerDocResult = await managerClient.GetAsync($"documents/{managerDoc.Id}");
			Console.WriteLine($"Status for managerGetManagerDocResult was {managerGetManagerDocResult.StatusCode}. Expected Ok");

			var adminGetManagerDocResult = await adminClient.GetAsync($"documents/{managerDoc.Id}");
			Console.WriteLine($"Status for adminGetManagerDocResult was {adminGetManagerDocResult.StatusCode}. Expected Ok");

			//Add User to Users group
			await adminClient.PostAsync($"users/{user.Id}/groups", new StringContent(JsonConvert.SerializeObject(new Group { Id = 3L }), Encoding.UTF8, "application/json"));
			userGetManagerDocResult = await userClient.GetAsync($"documents/{managerDoc.Id}");
			Console.WriteLine($"Status for userGetManagerDocResult was {userGetManagerDocResult.StatusCode} after adding user to users group. Expected Ok");

			var documentRetrieved = JsonConvert.DeserializeObject<Document>(await userGetManagerDocResult.Content.ReadAsStringAsync());
			Console.WriteLine($"Did document data retrieved match data sent: {documentRetrieved.Image == GetDocument()}");

			Console.WriteLine("Document retrieved metadata: ");
			Console.WriteLine($"Name: {documentRetrieved.Name}");
			Console.WriteLine($"Posted Date: {documentRetrieved.PostedDate}");
			Console.WriteLine($"Category: {documentRetrieved.Category}");
			Console.WriteLine($"Description: {documentRetrieved.Description}");

			//Except only user and admin to see this. admin due to privilege, user due to ownership. 
			var userDoc = new Document
			{
				Name = "user.png",
				Description = "User's doc",
				Category = "Testing",
				Image = GetDocument()
			};

			//user has been created but not added to any group yet, can't upload.
			var userCreateDocResult = await userClient.PostAsync("Documents", new StringContent(JsonConvert.SerializeObject(userDoc), Encoding.UTF8, "application/json"));
			Console.WriteLine($"Status for userCreateDocResult was {userCreateDocResult.StatusCode}. Expected: Forbidden");

			//Create new group that allows doc upload
			Group newGroupToAddDocs = new Group
			{
				Name = RandomString(12)
			};
			//Create new group
			var newGroupResult = await adminClient.PostAsync("groups", new StringContent(JsonConvert.SerializeObject(newGroupToAddDocs), Encoding.UTF8, "application/json"));
			//Store new group id
			newGroupToAddDocs = JsonConvert.DeserializeObject<Group>(await newGroupResult.Content.ReadAsStringAsync());
			Console.WriteLine($"Created new document uploader's group called {newGroupToAddDocs.Name}");
			//Add privilege to new group
			await adminClient.PostAsync($"groups/{newGroupToAddDocs.Id}/privileges", new StringContent(JsonConvert.SerializeObject(new Privilege { Id = (long)IUserRepository.PrivilegeType.UploadDocument }), Encoding.UTF8, "application/json"));

			//Add user to new group
			await adminClient.PostAsync($"users/{user.Id}/groups", new StringContent(JsonConvert.SerializeObject(newGroupToAddDocs), Encoding.UTF8, "application/json"));

			userCreateDocResult = await userClient.PostAsync("Documents", new StringContent(JsonConvert.SerializeObject(userDoc), Encoding.UTF8, "application/json"));
			Console.WriteLine($"Status for userCreateDocResult was {userCreateDocResult.StatusCode} after adding user to new 'Document Uploaders' group. Expected: Created");
			userDoc = JsonConvert.DeserializeObject<Document>(await userCreateDocResult.Content.ReadAsStringAsync());
			Console.WriteLine($"Created user doc with id {userDoc.Id}");

			var userGetUserDocResult = await userClient.GetAsync($"documents/{userDoc.Id}");
			Console.WriteLine($"Status for userGetUserDocResult was {userGetUserDocResult.StatusCode}. Expected Ok");

			var managerGetUserDocResult = await managerClient.GetAsync($"documents/{userDoc.Id}");
			Console.WriteLine($"Status for managerGetUserDocResult was {managerGetUserDocResult.StatusCode}. Expected Forbidden.");

			var adminGetUserDocResult = await adminClient.GetAsync($"documents/{userDoc.Id}");
			Console.WriteLine($"Status for adminGetUserDocResult was {adminGetUserDocResult.StatusCode}. Expected Ok");

			documentRetrieved = JsonConvert.DeserializeObject<Document>(await userGetUserDocResult.Content.ReadAsStringAsync());

			Console.WriteLine($"Did document data retrieved match data sent: {documentRetrieved.Image == GetDocument()}");
			Console.WriteLine("Document retrieved metadata: ");
			Console.WriteLine($"Name: {documentRetrieved.Name}");
			Console.WriteLine($"Posted Date: {documentRetrieved.PostedDate}");
			Console.WriteLine($"Category: {documentRetrieved.Category}");
			Console.WriteLine($"Description: {documentRetrieved.Description}");

			//Remove user from users group and confirm that user can no longer download files
			await adminClient.DeleteAsync($"users/{user.Id}/groups/3");
			userGetManagerDocResult = await userClient.GetAsync($"documents/{managerDoc.Id}");
			Console.WriteLine($"Status for userGetManagerDocResult was {userGetManagerDocResult.StatusCode} after removing user from users group. Expected Forbidden");

			//Remove privilege from Doc Uploader's group and confirm that user can no longer download files
			await adminClient.DeleteAsync($"groups/{newGroupToAddDocs.Id}/privileges/{(long)IUserRepository.PrivilegeType.UploadDocument}");
			userDoc.Id = 0;
			userDoc.Image = GetDocument();
			userCreateDocResult = await userClient.PostAsync("Documents", new StringContent(JsonConvert.SerializeObject(userDoc), Encoding.UTF8, "application/json"));
			Console.WriteLine($"Status for userCreateDocResult was {userCreateDocResult.StatusCode} after removing 'Upload Document' privilege from 'Document Uploaders' group. Expected: Forbidden");

			//Update user and see if new password lets us get a token
			string oldManagerPassword = manager.Password;
			manager.Password = "helicopter";
			await adminClient.PutAsync($"users/{manager.Id}", new StringContent(JsonConvert.SerializeObject(manager), Encoding.UTF8, "application/json"));
			tokenRes = await client.PostAsync($"token", new StringContent(JsonConvert.SerializeObject(new LoginModel { Username = manager.Username, Password = manager.Password }), Encoding.UTF8, "application/json"));
			Console.WriteLine($"Manager log in with new password {manager.Password} returned status {tokenRes.StatusCode}. Expected Ok");
			tokenRes = await client.PostAsync($"token", new StringContent(JsonConvert.SerializeObject(new LoginModel { Username = manager.Username, Password = oldManagerPassword }), Encoding.UTF8, "application/json"));
			Console.WriteLine($"Manager log in with old password {oldManagerPassword} returned status {tokenRes.StatusCode}. Expected Unauthorized");

			Console.ReadLine();
		}
		private static string RandomString(int length)
		{
			StringBuilder sb = new StringBuilder();
			char[] chars = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
			for(int i = 0;i < length; i++)
			{
				sb.Append(chars[r.Next(0, chars.Length)]);
			}
			return sb.ToString();
		}
		private static string GetDocument()
		{
			return "iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAIAAADTED8xAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATgSURBVHhe7dPdbWvnFUXRW0vqcZXpwF3lOQ3kBzCCeAImxcNDHkprAONlGr7iJ3GvX7/+/g/Y1YYpbZjShiltmNKGKW2Y0oYpbZjShiltmNKGKW2Y0oYpbZjShiltmNKGKW2Y0oYpbZjShiltmNKGKW2Y0oYpbZjShiltmNKGKW2Y0oYpbZjShiltmNKGKW2Y0oY3+tc/f92Q//kl2vB6OfS78s/P1IaXyVkfkB94gja8Rk75sPzYZ7XhbLngU+QjjmvDqXK4J8oHHdSG8+RkXyGf+LA2nCSX+iL50Ie14Qw505fKRz+mDU/Lgb5BHvCANjwnp/k2ecZXteE5uct3yku+pA1PyEW+WR7zJW04Kud4iTzpvjYclVu8RJ50XxuOyi1eJa+6ow2H5AovlIfd0YZDcoUXysPuaMMhucIL5WF3tOFxOcHL5Xm3tOFxub/L5Xm3tOFxub/L5Xm3tOFxub/L5Xm3tOFxub/L5Xm3tOFxub/L5Xm3tOFxub/L5Xm3tOFxub/L5Xm3tOFxub/L5Xm3tOGQnOC18rZb2nBITvBCedgdbTgkV3ihPOyONhySK7xQHnZHG47KIV4iT7qvDUflFi+RJ93XhifkHN8sj/mSNjwnR/lOecmXtOE5Ocq3yTO+qg1Py2m+QR7wgDacIQf6Uvnox7ThJDnT18nnPqYNJ8mZvkg+9GFtOFXu9UT5oIPacLYc7inyEce14QVyvk/KD39KG14md3xAfuAJ2vB6Oeu78s/P1IY3yqFH/ueXaMOUNkxpw5Q2TGnDlDZMacOUNkxpw5Q2TGnDlDZMacOUNkxpw5Q2TGnDlDZMacOUNkxpw5Q2TGnDlDZMacOUNkxpw5Q2TGnDlDZMacOUNkxpw5Q2TGnDlDZMacOUNkxpw5Q2n+Fvv/92QH4I97V5sZzsm+UxGMCr5PI+XB4/pM3jckw/Q37HH6vNPTmUBfkL/Cht/iynwH/kT/S9tefly+a2/PW+n/aefKMclj/s99AekK+NV8jf/HO1f6h8PbxNvoiP0/4p8jXwCfIdfYT2d5Y/Nx8rX9yV2t9K/qx8O/lCL9D+ePkL8mPki36T9kfKX4qfLd/+a7U/Rv4oDMpJvET7Ovnl4X9yKmdqv1d+T7gt93OC9uvlV4IDclTHtV8jr4dT5MyOaJ8nb4XXye09oP2cPAveLAd5X/txeQF8glzpX2p/TT4MPlZOt9p/LT8Xvpfc8x/a/yf/Hn6GP935n8LRM+OPm3f3LPvvAPKfYMevNEwxAKYZANMMgGkGwDQDYJoBMM0AmGYATDMAphkA0wyAaQbANANgmgEwzQCYZgBMMwCmGQDTDIBpBsA0A2CaATDNAJhmAEwzAKYZANMMgGkGwDQDYJoBMM0AmGYATDMAphkA0wyAaQbANANgmgEwzQCYZgBMMwCmGQDTDIBpBsA0A2CaATDNAJhmAEwzAKYZANMMgGkGwDQDYJoBMM0AmGYATDMAphkA0wyAaQbANANgmgEwzQCYZgBMMwCmGQDTDIBpBsA0A2CaATDNAJhmAEwzAKYZANMMgGkGwDQDYJoBMM0AmGYATDMAphkA0wyAaQbANANgmgEwzQCYZgBMMwCmGQDTDIBpBsA0A2CaATDNAJhmAEwzAKYZANMMgGkGwDQDYJoBMM0AGPb7b/8GK1fd+/aH97sAAAAASUVORK5CYII=";
		}
	}
}
