CREATE OR REPLACE FUNCTION docmanager.document_get_all()
    RETURNS SETOF docmanager.documents
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
posted_date,
name,
description,
category,
"image",
created_date,
modified_date,
accessed_date,
groups,
"user_id",
users
FROM docmanager.documents;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.document_get(
	_Id bigint
)
    RETURNS SETOF docmanager.documents
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
posted_date,
name,
description,
category,
"image",
created_date,
modified_date,
accessed_date,
groups,
"user_id",
users
FROM docmanager.documents
WHERE id = _Id
LIMIT 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.document_insert(
	_PostedDate timestamp without time zone,
	_Name text,
	_Description text,
	_Category text,
	_Image bytea,
	_CreatedDate timestamp without time zone,
	_ModifiedDate timestamp without time zone,
	_AccessedDate timestamp without time zone,
	_Groups bigint[],
	_UserId bigint,
	_Users bigint[]
)
    RETURNS bigint
    LANGUAGE 'plpgsql'
AS 
$BODY$
DECLARE
NewId bigint := 0;
BEGIN
INSERT INTO docmanager.documents(posted_date,name,description,category,"image",created_date,modified_date,accessed_date,groups,"user_id",users)
VALUES (_PostedDate, _Name, _Description, _Category, _Image, _CreatedDate, _ModifiedDate, _AccessedDate, _Groups, _UserId, _Users)
RETURNING id INTO NewId;
RETURN NewId;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.document_update(
	_Id bigint,
	_PostedDate timestamp without time zone,
	_Name text,
	_Description text,
	_Category text,
	_Image bytea,
	_CreatedDate timestamp without time zone,
	_ModifiedDate timestamp without time zone,
	_AccessedDate timestamp without time zone,
	_Groups bigint[],
	_UserId bigint,
	_Users bigint[]
)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
UPDATE docmanager.documents 
SET
posted_date = _PostedDate,
name = _Name,
description = _Description,
category = _Category,
"image" = _Image,
created_date = _CreatedDate,
modified_date = _ModifiedDate,
accessed_date = _AccessedDate,
groups = _Groups,
"user_id" = _UserId,
users = _Users
WHERE
id = _Id;
RETURN 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.document_delete(
	_Id bigint
)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
DECLARE
BEGIN
DELETE FROM docmanager.documents WHERE id = _Id;
RETURN 1;
END;
$BODY$;