CREATE OR REPLACE FUNCTION docmanager.privilege_get_all()
    RETURNS SETOF docmanager.privileges
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
name,
created_date,
modified_date
FROM docmanager.privileges;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.privilege_get(
	_Id bigint
)
    RETURNS SETOF docmanager.privileges
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
name,
created_date,
modified_date
FROM docmanager.privileges
WHERE id = _Id
LIMIT 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.privilege_insert(
	_Name text,
	_CreatedDate timestamp without time zone,
	_ModifiedDate timestamp without time zone
)
    RETURNS bigint
    LANGUAGE 'plpgsql'
AS 
$BODY$
DECLARE
NewId bigint := 0;
BEGIN
INSERT INTO docmanager.privileges(name, created_date, modified_date)
VALUES (_Name, _CreatedDate, _ModifiedDate)
RETURNING id INTO NewId;
RETURN NewId;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.privilege_update(
	_Id bigint,
	_Name text,
	_CreatedDate timestamp without time zone,
	_ModifiedDate timestamp without time zone
)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
UPDATE docmanager.privileges 
SET
name = _Name,
created_date = _CreatedDate,
modified_date = _ModifiedDate
WHERE
id = _Id;
RETURN 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.privilege_delete(
	_Id bigint
)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
DECLARE
BEGIN
DELETE FROM docmanager.privileges WHERE id = _Id;
RETURN 1;
END;
$BODY$;