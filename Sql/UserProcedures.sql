CREATE OR REPLACE FUNCTION docmanager.user_get_all()
    RETURNS SETOF docmanager.users
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
username,
password_hash,
password_salt,
name,
created_date,
modified_date
FROM docmanager.users;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.user_get(
	_Id bigint
)
    RETURNS SETOF docmanager.users
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
username,
password_hash,
password_salt,
name,
created_date,
modified_date
FROM docmanager.users
WHERE id = _Id
LIMIT 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.user_insert(
	_Username text,
	_PasswordHash text,
	_PasswordSalt text,
	_Name text,
	_CreatedDate timestamp without time zone,
	_ModifiedDate timestamp without time zone
)
    RETURNS bigint
    LANGUAGE 'plpgsql'
AS 
$BODY$
DECLARE
NewId bigint := 0;
BEGIN
INSERT INTO docmanager.users(username, password_hash,password_salt, name, created_date, modified_date)
VALUES (_Username, _PasswordHash, _PasswordSalt, _Name, _CreatedDate, _ModifiedDate)
RETURNING id INTO NewId;
RETURN NewId;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.user_update(
	_Id bigint,
	_Username text,
	_PasswordHash text,
	_PasswordSalt text,
	_Name text,
	_CreatedDate timestamp without time zone,
	_ModifiedDate timestamp without time zone
)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
UPDATE docmanager.users 
SET
username = _Username,
password_hash = _PasswordHash,
password_salt = _PasswordSalt,
name = _Name,
created_date = _CreatedDate,
modified_date = _ModifiedDate
WHERE
id = _Id;
RETURN 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.user_delete(
	_Id bigint
)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
DECLARE
BEGIN
DELETE FROM docmanager.users WHERE id = _Id;
RETURN 1;
END;
$BODY$;


CREATE OR REPLACE FUNCTION docmanager.user_get_by_username(_InUsername text)
    RETURNS SETOF docmanager.users
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
username,
password_hash,
password_salt,
name,
created_date,
modified_date
FROM docmanager.users
WHERE username = _InUsername
LIMIT 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.user_get_groups(_InUserId bigint)
    RETURNS SETOF docmanager.groups
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
g.id,
g.name,
g.created_date,
g.modified_date
FROM docmanager.user_groups ug INNER JOIN docmanager.groups g ON g.id = ug.group_id
WHERE ug."user_id" = _InUserId;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.user_add_to_group(_InUserId bigint, _InGroupId bigint)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
INSERT INTO docmanager.user_groups ("user_id", group_id) values (_InUserId, _InGroupId);
RETURN 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.user_remove_from_group(_InUserId bigint, _InGroupId bigint)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
DELETE FROM docmanager.user_groups where "user_id" = _InUserId and group_id = _InGroupId;
RETURN 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.user_get_documents(_InUserId bigint)
    RETURNS SETOF docmanager.documents
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
posted_date,
name,
description,
category,
"image",
created_date,
modified_date,
accessed_date,
groups,
"user_id",
users
FROM docmanager.documents
WHERE "user_id" = _InUserId;
END;
$BODY$;







