CREATE OR REPLACE FUNCTION docmanager.group_get_all()
    RETURNS SETOF docmanager.groups
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
name,
created_date,
modified_date
FROM docmanager.groups;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.group_get(
	_Id bigint
)
    RETURNS SETOF docmanager.groups
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
name,
created_date,
modified_date
FROM docmanager.groups
WHERE id = _Id
LIMIT 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.group_insert(
	_Name text,
	_CreatedDate timestamp without time zone,
	_ModifiedDate timestamp without time zone
)
    RETURNS bigint
    LANGUAGE 'plpgsql'
AS 
$BODY$
DECLARE
NewId bigint := 0;
BEGIN
INSERT INTO docmanager.groups(name, created_date, modified_date)
VALUES (_Name, _CreatedDate, _ModifiedDate)
RETURNING id INTO NewId;
RETURN NewId;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.group_update(
	_Id bigint,
	_Name text,
	_CreatedDate timestamp without time zone,
	_ModifiedDate timestamp without time zone
)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
UPDATE docmanager.groups 
SET
name = _Name,
created_date = _CreatedDate,
modified_date = _ModifiedDate
WHERE
id = _Id;
RETURN 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.group_delete(
	_Id bigint
)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
DECLARE
BEGIN
DELETE FROM docmanager.groups WHERE id = _Id;
RETURN 1;
END;
$BODY$;


CREATE OR REPLACE FUNCTION docmanager.group_add_privilege(_InGroupId bigint, _InPrivilegeId bigint)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
INSERT INTO docmanager.group_privileges (group_id, privilege_id) VALUES (_InGroupId, _InPrivilegeId);
RETURN 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.group_remove_privilege(_InGroupId bigint, _InPrivilegeId bigint)
    RETURNS int
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
DELETE FROM docmanager.group_privileges WHERE group_id = _InGroupId AND privilege_id = _InPrivilegeId;
RETURN 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.group_get_by_name(_InName text)
    RETURNS SETOF docmanager.groups
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
id,
name,
created_date,
modified_date
FROM docmanager.groups
WHERE name = _InName
LIMIT 1;
END;
$BODY$;

CREATE OR REPLACE FUNCTION docmanager.group_get_privileges(_InGroupId bigint)
    RETURNS SETOF docmanager.groups
    LANGUAGE 'plpgsql'
AS 
$BODY$
BEGIN
RETURN QUERY SELECT 
p.id,
p.name,
p.created_date,
p.modified_date
FROM docmanager.group_privileges gp INNER JOIN docmanager.privileges p ON p.id = gp.privilege_id
WHERE gp.group_id = _InGroupId;
END;
$BODY$;